﻿using UnityEngine;

public class camSys : MonoBehaviour
{
    Vector2 f0start;
    Vector2 f1start;
    public GameObject cam;
    public float temp;

    void Update()
    {
        if (Input.touchCount < 2)
        {
            f0start = Vector2.zero;
            f1start = Vector2.zero;
        }
        if (Input.touchCount == 2) Zoom();
    }

    void Zoom()
    {
        if (f0start == Vector2.zero && f1start == Vector2.zero)
        {
            f0start = Input.GetTouch(0).position;
        }
        Vector2 f0position = Input.GetTouch(0).position;
        temp = f0start.x - f0position.x;
        cam.transform.rotation = Quaternion.Euler(0, temp, 0);
    }
}