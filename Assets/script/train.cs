﻿using UnityEngine;

public class train : MonoBehaviour
{
    private bool grab;

    public bool stop;
    public float realSpeed, speed,stopSpeed,tovar;
    public GameObject addGruz;

    private void Start()
    {
        grab = true;
    }

    void Update()
    {
        realSpeed = gameObject.GetComponent<Rigidbody>().velocity.z;
        if(realSpeed<25f)
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, speed));

        if (gameObject.GetComponent<Rigidbody>().velocity.z <= 0.03f && grab == true) addGruz.SetActive(true);
        else addGruz.SetActive(false);

        if (gameObject.GetComponent<Rigidbody>().velocity.z > 0 && stop==true)
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, stopSpeed));
        

        if (gameObject.GetComponent<Rigidbody>().velocity.z < 0)
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "stait") grab = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "stait") grab = false;
    }
    public void grabTowar()
    {
        tovar++;
        grab = false;
    }
}
