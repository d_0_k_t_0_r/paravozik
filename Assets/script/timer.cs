﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class timer : MonoBehaviour
{
    private Text text;
    private int m, s;
    public UI ui;
    
    private void Start()
    {
        text = this.gameObject.GetComponent<Text>();
        m = 5;
        s = 0;
    }
    public void startTimer()
    {
        StartCoroutine(Time());
    }
    private IEnumerator Time()
    {
        for (int i = 300; i > 0; i--)
        {
            yield return new WaitForSeconds(1);
            if (s == 0)
            {
                s = 59;
                m--;
            }
            else s--;

            if (s >= 10) text.text = m.ToString() + ":" + s.ToString();
            else text.text = m.ToString() + ":0" + s.ToString();
        }
        ui.StopGame();
    }
}
