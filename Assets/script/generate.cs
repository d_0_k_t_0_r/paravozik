﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generate : MonoBehaviour
{
    private float cor_z;
    public GameObject ter, ter_station;

    private void Start() => cor_z = 65f;


    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ter")
        {
            int r = Random.Range(0, 51);
            switch (r)
            {
                case 50:
                    Instantiate(ter_station, new Vector3(0,0, cor_z), Quaternion.identity);
                    break;
                default:
                    Instantiate(ter, new Vector3(0,0, cor_z), Quaternion.identity);
                    break;
            }
            cor_z += 4f;
        }
    }
}
