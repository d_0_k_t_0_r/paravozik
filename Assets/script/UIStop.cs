﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIStop : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public train train;

    public void OnPointerDown(PointerEventData eventData)
    {
        train.stop=true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        train.stop = false;
    }
}
