﻿using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    private bool onGame = false;

    public GameObject tovarUp, stopButton, timer, speedSlider, startButton, player,itog;
    public Text Speed, tovar,itogGruzov;

    private void Update()
    {
        if (onGame == true)
        {
            Speed.text = Math.Round(player.GetComponent<train>().realSpeed * 10f).ToString();
            tovar.text = player.GetComponent<train>().tovar.ToString();
            player.GetComponent<train>().speed = speedSlider.GetComponent<Slider>().value;
            speedSlider.SetActive(true);
            Speed.enabled = true;
            tovar.enabled = true;
            if (speedSlider.GetComponent<Slider>().value <= 0f) stopButton.SetActive(true);
            else stopButton.SetActive(false);
        }
        else
        {
            tovarUp.SetActive(false);
            stopButton.SetActive(false);
            speedSlider.SetActive(false);
            Speed.enabled = false;
            tovar.enabled = false;
        }
        
    }

    public void addGruz()
    {
        player.GetComponent<train>().tovar++;
    }

    public void StartGame()
    {
        startButton.SetActive(false);
        timer.GetComponent<timer>().startTimer();
        onGame = true;
    }
    public void StopGame()
    {
        train train = player.GetComponent<train>();
        onGame = false;
        speedSlider.GetComponent<Slider>().value = 0;
        itog.SetActive(true);
        if (train.tovar == 1)
            itogGruzov.text = "Вы собрали 1 груз";

        else if(train.tovar>1 && train.tovar < 5)
            itogGruzov.text = "Вы собрали " + train.tovar.ToString() + "груза";
        else
            itogGruzov.text = "Вы собрали " + train.tovar.ToString() + "грузов";
    }
    public void reStart()=> SceneManager.LoadScene("game");
}
